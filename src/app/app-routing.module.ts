import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './Components/admin/admin.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { DetailsComponent } from './Components/details/details.component';

const routes: Routes = [
  {path:"dashboard",component:DashboardComponent},
  {path:"details",component:DetailsComponent},
  {path:"",component:AdminComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
