export class VehicleDesc{
  year:number=0;
  brandId:number=0;
  modelId:number=0;
  description:string="";
  cc:string="";
  fuel:string="";
}
