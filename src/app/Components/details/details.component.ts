import { Component ,OnInit} from '@angular/core';
import { VehiclesDetails } from 'src/app/Class/detailsForTable';
import { DataSendingService } from 'src/app/Services/data-sending.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit{
  primaryData:VehiclesDetails=new VehiclesDetails();
  allVehicleData:VehiclesDetails[]=[];
  constructor(private send:DataSendingService){}
  ngOnInit(): void {
    this.primaryData=this.send.vehicleData;
    this.allVehicleData=this.send.allVehicleData;
  }

}
