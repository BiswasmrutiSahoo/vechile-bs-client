import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehiclesDetails } from 'src/app/Class/detailsForTable';
import { ApiService } from 'src/app/Services/api.service';
import { DataSendingService } from 'src/app/Services/data-sending.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{

allVehicleData:VehiclesDetails[]=[];

constructor(private api:ApiService,private router:Router,private send:DataSendingService){}
ngOnInit(): void {
  this.api.getAllVehicleData().subscribe((data:any)=>{
    this.allVehicleData=[];
    this.allVehicleData=data;
  })
}

onCardClick(data:VehiclesDetails){
this.send.vehicleData=data;
this.send.allVehicleData=[];
this.allVehicleData.forEach((a:VehiclesDetails)=>{
if(data.brandName==a.brandName && data!=a){
  this.send.allVehicleData.push(a);
}
})
this.router.navigateByUrl("/details");
}
}
