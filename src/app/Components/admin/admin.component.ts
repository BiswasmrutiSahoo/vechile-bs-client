import { Component,OnInit } from '@angular/core';
import { Brand } from 'src/app/Class/brand';
import { Model } from 'src/app/Class/model';
import { VehicleDesc } from 'src/app/Class/vehicleDesc';
import { VehiclesDetails } from 'src/app/Class/detailsForTable';
import { ApiService } from 'src/app/Services/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit{
  modelId:number=0;
  brandId:number=0;
  year:number=new Date().getFullYear();
  modelList:Model[]=[];
  brandList:Brand[]=[];
  yearList:number[]=[];
  selectedYear:number=0;
  description:string="";
  cc:string="";
  fuel:string="";
  checkBox:boolean=false;
  vehicleData:VehicleDesc=new VehicleDesc();
  allVehicleData:VehiclesDetails[]=[];

  constructor(private api:ApiService){}

  ngOnInit(): void {
    this.api.getBrandData().subscribe((data:any)=>{
      this.brandList=data;
    })
  }
  getModelData(){
    this.selectedYear=0;
    this.modelId=0;
    this.modelList=[];
    this.api.getModelData(this.brandId).subscribe((data:any)=>{
      this.modelList=data;
    })
  }
  getYear(){
    this.selectedYear=0;
    this.yearList=[];
    for(let a:number=0;a<10;a++){
      this.yearList.push(this.year-a);
    }
  }
  validate():boolean{
    let flag:boolean=true;
    let msg:string="";
    if(this.brandId==0 && flag ==true){
      flag=false;
      msg="Select a Brand";
    }
    if(this.modelId==0 && flag ==true){
      flag=false;
      msg="Select a Model";
    }
    if(this.year==0 && flag ==true){
      flag=false;
      msg="Select a Year";
    }
    if((this.description.length>250 ||this.description.length==0) && flag ==true){
      flag=false;
      if(this.description.length>250 ){
        msg="Description exceeding 250 charecters"
      }
      if(this.description.length==0){
      msg="please enter description";
      }
    }
    if(!(/^([0-9]{2,3})$/.test(this.cc)) && flag==true){
      flag=false;
      msg="please enter a valid cc";
    }
    if(this.fuel=="" && flag==true){
      flag=false;
      msg="please select a fuel type";
    }
    if(this.checkBox==false){
      flag=false;
      msg="please select the checkBox"
    }
    if(flag==false){
      alert(msg);
    }
    return flag;
  }
  onAddDetailClicked(){
    if(this.validate()){
    this.vehicleData.brandId=this.brandId;
    this.vehicleData.cc=this.cc;
    this.vehicleData.description=this.description;
    this.vehicleData.fuel=this.fuel;
    this.vehicleData.modelId=this.modelId;
    this.vehicleData.year=this.selectedYear;
    this.api.postVehicleData(this.vehicleData).subscribe((data:any)=>{
      alert(data.error.text);
    })
    console.log("data added sucessfully");
    this.api.getAllVehicleData().subscribe((data:any)=>{
      this.allVehicleData=[];
      this.allVehicleData=data;
    })
    }
  }
}
