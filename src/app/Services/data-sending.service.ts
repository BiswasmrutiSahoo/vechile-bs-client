import { Injectable } from '@angular/core';
import { VehiclesDetails } from '../Class/detailsForTable';

@Injectable({
  providedIn: 'root'
})
export class DataSendingService {
  vehicleData:VehiclesDetails=new VehiclesDetails();
  allVehicleData:VehiclesDetails[]=[];
  constructor() { }

}
