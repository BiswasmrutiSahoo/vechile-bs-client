import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { VehicleDesc } from '../Class/vehicleDesc';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  getBrandUrl="https://localhost:7057/Common/GetBrands";
  getModelUrl="https://localhost:7057/Common/GetModels?id=";
  getAllVehicleDataUrl="https://localhost:7057/Common/getAllVehicle";
  postVehicleDataUrl="https://localhost:7057/Common/postVehicleData";

  constructor(private Http:HttpClient) { }
  getBrandData(){
    return this.Http.get(this.getBrandUrl);
  }
  getModelData(id:Number){
    return this.Http.get(this.getModelUrl+id);
  }
  postVehicleData(data:VehicleDesc){
    return this.Http.post(this.postVehicleDataUrl,data);
  }
  getAllVehicleData(){
    return this.Http.get(this.getAllVehicleDataUrl);
  }

}
