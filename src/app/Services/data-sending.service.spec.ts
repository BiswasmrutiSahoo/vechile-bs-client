import { TestBed } from '@angular/core/testing';

import { DataSendingService } from './data-sending.service';

describe('DataSendingService', () => {
  let service: DataSendingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataSendingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
